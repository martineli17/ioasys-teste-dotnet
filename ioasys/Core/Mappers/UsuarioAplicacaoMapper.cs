﻿using AutoMapper;
using ioasys.Core.Configuracao.Autorizacao;
using ioasys.Core.DTO;
using System;

namespace ioasys.Core.Mappers
{
    public class UsuarioAplicacaoMapper : Profile
    {
        public UsuarioAplicacaoMapper()
        {
            CreateMap<UsuarioAplicacaoDTO, UsuarioIdentity>()
               .ForMember(dest => dest.Email, options => options.MapFrom(src => src.Email))
               .ForMember(dest => dest.UserName, options => options.MapFrom(src => src.Email))
               .ForMember(dest => dest.Nome, options => options.MapFrom(src => src.Nome))
               .AfterMap((src, dest) => dest.Id = Guid.NewGuid().ToString());
            CreateMap<UsuarioAplicacaoEdicaoDTO, UsuarioIdentity>()
               .ForMember(dest => dest.Email, options => options.MapFrom(src => src.Email))
               .ForMember(dest => dest.UserName, options => options.MapFrom(src => src.Email))
               .ForMember(dest => dest.Id, options => options.MapFrom(src => src.Id))
               .ForMember(dest => dest.Nome, options => options.MapFrom(src => src.Nome));
        }
    }
}
