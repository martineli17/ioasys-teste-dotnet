﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace ioasys.Core.Configuracao.Autorizacao
{
    public class ContextIdentity : IdentityDbContext<UsuarioIdentity>
    {
        public ContextIdentity(DbContextOptions<ContextIdentity> options) : base(options)
        {

        }

        public DbSet<UsuarioIdentity> Usuario { get; set; }
    }
}
