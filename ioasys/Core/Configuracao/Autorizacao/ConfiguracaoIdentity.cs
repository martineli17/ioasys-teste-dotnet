﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using NetDevPack.Identity;
using NetDevPack.Identity.Jwt;
using NetDevPack.Identity.User;
using System;

namespace ioasys.Core.Configuracao.Autorizacao
{
    public static class ConfiguracaoIdentity
    {
        public static IServiceCollection AddIdentityCustom(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddAspNetUserConfiguration();
            services.AddDbContext<ContextIdentity>(options => options.UseSqlServer(configuration.GetConnectionString("SQLSERVER")));
            services.AddCustomIdentity<UsuarioIdentity>(options =>
            {
                options.SignIn.RequireConfirmedEmail = false;
                options.Lockout.MaxFailedAccessAttempts = 5;
                options.Lockout.DefaultLockoutTimeSpan = TimeSpan.FromMinutes(30);
            }).AddEntityFrameworkStores<ContextIdentity>().AddDefaultTokenProviders().AddErrorDescriber<IdentityMensagensBR>();
            services.AddJwtConfiguration(configuration, "AppSettings");
            return services;
        }
    }
}
