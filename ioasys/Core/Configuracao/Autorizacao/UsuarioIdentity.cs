﻿using Microsoft.AspNetCore.Identity;

namespace ioasys.Core.Configuracao.Autorizacao
{
    public class UsuarioIdentity : IdentityUser
    {
        public bool Ativo { get; set; }
        public string Nome { get; set; }
        public void Ativar() => Ativo = true;
        public void Desativar() => Ativo = false;
    }
}
