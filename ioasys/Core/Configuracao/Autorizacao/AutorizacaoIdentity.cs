﻿using Crosscuting.Servicos.Notificadores;
using Dominio.Service.Validadores.Base;
using ioasys.Core.DTO;
using ioasys.Core.TiposValores;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Options;
using NetDevPack.Identity.Jwt;
using NetDevPack.Identity.Jwt.Model;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace ioasys.Core.Configuracao.Autorizacao
{
    public class AutorizacaoIdentity : IAutorizacaoIdentity
    {
        private readonly SignInManager<UsuarioIdentity> _signInManager;
        private readonly UserManager<UsuarioIdentity> _userManager;
        private readonly AppJwtSettings _appJwtSettings;
        private readonly INotificador _notificator;

        public AutorizacaoIdentity(SignInManager<UsuarioIdentity> signInManager, UserManager<UsuarioIdentity> userManager,
               IOptions<AppJwtSettings> appJwtSettings, INotificador notificator)
        {
            _signInManager = signInManager;
            _userManager = userManager;
            _notificator = notificator;
            _appJwtSettings = appJwtSettings.Value;
        }

        public async Task CadastrarAsync(UsuarioIdentity usuario, RoleUsuario role, string senha)
        {
            usuario.Ativar();
            if (!await (RegistrarUsuario(usuario, senha))) return;
            if (await (RegistrarClaim(usuario, role))) return;
            await _userManager.DeleteAsync(await _userManager.FindByEmailAsync(usuario.Email));
        }

        public async Task EditarAsync(UsuarioIdentity usuario)
        {
            var usuarioEdicao = await _userManager.FindByIdAsync(usuario.Id);
            if (usuarioEdicao is null)
            {
                _notificator.Inserir(MensagensValidador.NotFound);
                return;
            }
            usuarioEdicao.UserName = usuario.UserName;
            usuarioEdicao.Email = usuario.Email;
            usuarioEdicao.Nome = usuario.Nome;
            await _userManager.UpdateAsync(usuarioEdicao);
        }

        public async Task AtivarAsync(string idUsuario)
        {
            var usuario = await _userManager.FindByIdAsync(idUsuario);
            await _userManager.AddClaimAsync(usuario, new Claim("ATIVO", "1"));
            usuario.Ativar();
            var resultadoAtivacao = await _userManager.UpdateAsync(usuario);
            if (resultadoAtivacao.Succeeded) return;
            InserirErrosIdentity(resultadoAtivacao);
        }

        public async Task DesativarAsync(string idUsuario)
        {
            var usuario = await _userManager.FindByIdAsync(idUsuario);
            await _userManager.RemoveClaimAsync(usuario, new Claim("ATIVO", "1"));
            usuario.Desativar();
            var resultadoAtivacao = await _userManager.UpdateAsync(usuario);
            if (resultadoAtivacao.Succeeded) return;
            InserirErrosIdentity(resultadoAtivacao);
        }

        public async Task<bool> LoginAsync(UsuarioLoginDTO login)
        {
            var resultLogin = await _signInManager.PasswordSignInAsync(login.Email, login.Senha, true, true);
            if (resultLogin.Succeeded) return true;
            if (resultLogin.IsLockedOut) 
            { 
                _notificator.Inserir("Usuário temporariamente bloquado.", EnumTipoNotificacao.Warning);
                return false; 
            };
            _notificator.Inserir("Usuário ou Senha inválido.", EnumTipoNotificacao.Warning);
            return false;
        }

        public UserResponse<string> GerarToken(string email) =>
            new JwtBuilder<UsuarioIdentity>()
                .WithUserManager(_userManager)
                .WithJwtSettings(_appJwtSettings)
                .WithEmail(email)
                .WithJwtClaims()
                .WithUserClaims()
                .BuildUserResponse();

        #region Métodos Privados
        private async Task<bool> RegistrarUsuario(UsuarioIdentity usuario, string senha)
        {
            var resultadoRegistro = await _userManager.CreateAsync(usuario, senha);
            if (!resultadoRegistro.Succeeded)
            {
                InserirErrosIdentity(resultadoRegistro);
                return false;
            }
            return true;
        }
        private async Task<bool> RegistrarClaim(UsuarioIdentity usuario, RoleUsuario role)
        {
            var resultadoClaim = await _userManager.AddClaimsAsync(await _userManager.FindByEmailAsync(usuario.Email), GerarClaims(usuario, role));
            if (!resultadoClaim.Succeeded)
            {
                InserirErrosIdentity(resultadoClaim);
                return false;
            }
            return true;
        }
        private void InserirErrosIdentity(IdentityResult resultado) =>
            _notificator.Inserir(resultado.Errors.Select(x => x.Description), EnumTipoNotificacao.Warning);
        private List<Claim> GerarClaims(UsuarioIdentity usuario, RoleUsuario role)
        {
            return new List<Claim> 
            { 
                new Claim("ID", usuario.Id),
                new Claim("ROLE", role.ToString()),
                new Claim("ATIVO", "1"),
            };
        }
        #endregion
    }
}
