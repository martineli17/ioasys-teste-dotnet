﻿using ioasys.Core.DTO;
using ioasys.Core.TiposValores;
using NetDevPack.Identity.Jwt.Model;
using System;
using System.Threading.Tasks;

namespace ioasys.Core.Configuracao.Autorizacao
{
    public interface IAutorizacaoIdentity
    {
        Task CadastrarAsync(UsuarioIdentity usuario, RoleUsuario role, string senha);
        Task DesativarAsync(string idUsuario);
        Task AtivarAsync(string idUsuario);
        Task<bool> LoginAsync(UsuarioLoginDTO login);
        UserResponse<string> GerarToken(string email);
        Task EditarAsync(UsuarioIdentity usuario);
    }
}
