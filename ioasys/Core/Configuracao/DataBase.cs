﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Repositorio.Contexto;

namespace ioasys.Core.Configuracao
{
    public static class DataBase
    {
        public static IServiceCollection AddDataBase(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbContext<Context>(options => options
                                                .UseLoggerFactory(LoggerFactory.Create(p => p.AddConsole()))
                                                .EnableSensitiveDataLogging().EnableDetailedErrors()
                                                .UseSqlServer(configuration.GetConnectionString("SQLSERVER"))
                                                .UseQueryTrackingBehavior(QueryTrackingBehavior.NoTracking));

            return services;
        }
    }
}
