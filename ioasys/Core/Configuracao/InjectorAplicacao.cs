﻿using AutoMapper;
using Crosscuting.Servicos.Notificadores;
using Dominio.Contratos.Servicos;

namespace ioasys.Core.Configuracao
{
    public class InjectorAplicacao
    {
        public readonly IMapper Mapper;
        public readonly INotificador Notificador;
        public readonly IUsuarioAplicacaoService UsuarioAplicacao;

        public InjectorAplicacao(IMapper mapper, INotificador notificador, IUsuarioAplicacaoService usuarioAplicacao)
        {
            Mapper = mapper;
            Notificador = notificador;
            UsuarioAplicacao = usuarioAplicacao;
        }
    }
}
