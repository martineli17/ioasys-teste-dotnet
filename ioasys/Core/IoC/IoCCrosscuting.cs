﻿using Crosscuting.Servicos.Notificadores;
using Microsoft.Extensions.DependencyInjection;

namespace ioasys.Core.IoC
{
    public static class IoCCrosscuting
    {
        public static IServiceCollection AddIoCCrosscuting(this IServiceCollection services)
        {
            services.AddScoped<INotificador, Notificador>();
            return services;
        }
    }
}
