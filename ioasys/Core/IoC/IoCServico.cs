﻿using Dominio.Contratos.Servicos;
using Dominio.Service.Base;
using Dominio.Service.Servicos;
using Dominio.Service.Validadores.Base;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace ioasys.Core.IoC
{
    public static class IoCServico
    {
        public static IServiceCollection AddIoCService(this IServiceCollection services)
        {
            services.AddScoped<IAtorService, AtorService>().AddScoped(x => new Lazy<IAtorService>(x.GetService<IAtorService>()));
            services.AddScoped<IAtorFilmeService, AtorFilmeService>().AddScoped(x => new Lazy<IAtorFilmeService>(x.GetService<IAtorFilmeService>()));
            services.AddScoped<IDiretorService, DiretorService>().AddScoped(x => new Lazy<IDiretorService>(x.GetService<IDiretorService>()));
            services.AddScoped<IFilmeService, FilmeService>().AddScoped(x => new Lazy<IFilmeService>(x.GetService<IFilmeService>()));
            services.AddScoped<IFilmeFacadeService, FilmeFacadeService>().AddScoped(x => new Lazy<IFilmeFacadeService>(x.GetService<IFilmeFacadeService>()));
            services.AddScoped<IValidador, Validador>();
            services.AddScoped<InjectorServico>();
            return services;
        }
    }
}
