﻿using Dominio.Contratos.Servicos;
using ioasys.Core.Configuracao;
using ioasys.Core.Configuracao.Autorizacao;
using ioasys.Core.Servicos;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace ioasys.Core.IoC
{
    public static class IoCAplicacao
    {
        public static IServiceCollection AddIoCAplicaco(this IServiceCollection services)
        {
            services.AddScoped<IUsuarioAplicacaoService, UsuarioAplicacaoService>().AddScoped(x => new Lazy<IUsuarioAplicacaoService>(x.GetService<IUsuarioAplicacaoService>()));
            services.AddScoped<IAutorizacaoIdentity, AutorizacaoIdentity>();
            services.AddScoped<InjectorAplicacao>();
            return services;
        }
    }
}
