﻿using Dominio.Contratos.Base;
using Dominio.Contratos.Repositorios;
using Microsoft.Extensions.DependencyInjection;
using Repositorio.Repositorios;
using Repositorio.Repositorios.Base;
using System;

namespace ioasys.Core.IoC
{
    public static class IoCRepositorio
    {
        public static IServiceCollection AddIoCRepositorio(this IServiceCollection services)
        {
            services.AddScoped<IAtorRepositorio, AtorRepositorio>();
            services.AddScoped<IAtorFilmeRepositorio, AtorFilmeRepositorio>();
            services.AddScoped<IAvaliacaoRepositorio, AvaliacaoRepositorio>().AddScoped(x => new Lazy<IAvaliacaoRepositorio>(x.GetService<IAvaliacaoRepositorio>()));
            services.AddScoped<IDiretorRepositorio, DiretorRepositorio>();
            services.AddScoped<IFilmeRepositorio, FilmeRepositorio>().AddScoped(x => new Lazy<IFilmeRepositorio>(x.GetService<IFilmeRepositorio>()));
            services.AddScoped<IUnitOfWork, UnitOfWork>();
            services.AddScoped<InjectorRepositorio>();
            return services;
        }
    }
}
