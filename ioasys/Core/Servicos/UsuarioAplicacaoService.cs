﻿using Dominio.Contratos.Servicos;
using Dominio.DTO;
using ioasys.Core.Configuracao.Autorizacao;
using ioasys.Core.TiposValores;
using Microsoft.AspNetCore.Http;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ioasys.Core.Servicos
{
    public class UsuarioAplicacaoService : IUsuarioAplicacaoService
    {
        private readonly IHttpContextAccessor _accessor;
        private readonly ContextIdentity _context;
        public UsuarioAplicacaoService(IHttpContextAccessor accessor, ContextIdentity context)
        {
            _accessor = accessor;
            _context = context;
        }
        public string RetornarIdUsuarioLogado() => _accessor.HttpContext.User.Claims.Where(x => x.Type == "ID").FirstOrDefault().Value;
        public Task<List<UsuarioDTO>> RetornarUsuariosAtivos() =>
            Task.FromResult(_context.Users.Where(x => x.Ativo 
                            && _context.UserClaims.FirstOrDefault(c => c.UserId == x.Id 
                            && c.ClaimType == "ROLE").ClaimValue != RoleUsuario.ADMINISTRADOR.ToString())
                         .Select(RetornarDTOdeUsuario).ToList());

        #region Métodos Privados
        private UsuarioDTO RetornarDTOdeUsuario(UsuarioIdentity usuario) =>
            new UsuarioDTO 
            {
                Nome = usuario.Nome,
                Id = usuario.Id,
                Ativo = usuario.Ativo,
                Email = usuario.Email
            };
        #endregion
    }
}
