﻿namespace ioasys.Core.DTO
{
    public class ResponseDTO<TResponse>
    {
        public TResponse Dados { get; set; }
    }
}
