﻿using ioasys.Core.TiposValores;
using System.ComponentModel.DataAnnotations;

namespace ioasys.Core.DTO
{
    public class UsuarioAplicacaoDTO
    {
        [Required]
        public string Nome { get; set; }
        [Required]
        [EmailAddress(ErrorMessage = "E-mail inválido")]
        public string Email { get; set; }
        [Required]
        [EnumDataType(typeof(RoleUsuario), ErrorMessage = "Role inválida")]
        public RoleUsuario Role { get; set; }
        [Required]
        public string Senha { get; set; }
    }

    public class UsuarioAplicacaoEdicaoDTO
    {
        [Required]
        public string Id { get; set; }
        public string Nome { get; set; }
        [EmailAddress(ErrorMessage = "E-mail inválido")]
        public string Email { get; set; }
    }

    public class UsuarioLoginDTO
    {
        [Required]
        [EmailAddress(ErrorMessage = "E-mail inválido")]
        public string Email { get; set; }
        [Required]
        public string Senha { get; set; }
    }
}
