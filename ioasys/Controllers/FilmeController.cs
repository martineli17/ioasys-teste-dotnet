﻿using Dominio.Contratos.Servicos;
using Dominio.DTO;
using ioasys.Core.Configuracao;
using ioasys.Core.Constantes;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using NetDevPack.Identity.Authorization;
using System;
using System.Threading.Tasks;

namespace ioasys.Controllers
{
    [Route(UrlApi.BaseUrl + "filme")]
    [Authorize]
   
    public class FilmeController : BaseAtivoController
    {
        private readonly Lazy<IFilmeFacadeService> _filmeFacadeService;
        private readonly Lazy<IFilmeService> _filmeService;
        private readonly Lazy<IUsuarioAplicacaoService> _usuarioService;
        public FilmeController(Lazy<IFilmeService> filmeService, Lazy<IFilmeFacadeService> filmeFacadeService, 
                                Lazy<IUsuarioAplicacaoService> usuarioService, InjectorAplicacao injector)
            :base(injector)
        {
            _filmeService = filmeService;
            _filmeFacadeService = filmeFacadeService;
            _usuarioService = usuarioService;
        }

        [CustomAuthorize("ROLE", "ADMINISTRADOR")]
        [HttpPost]
        public async Task<ActionResult> Adicionar([FromBody] FilmeDTO filmeDTO)
        {
            filmeDTO.IdUsuarioCadastro = _usuarioService.Value.RetornarIdUsuarioLogado();
            var resultadoAdicao = await _filmeFacadeService.Value.AdicionarAsync(filmeDTO);
            return await base.CreateResponse(resultadoAdicao, 201);
        }

        [HttpGet]
        public async Task<ActionResult> Buscar([FromQuery] FilmeFiltroDTO filmeFiltroDTO, [FromQuery] bool detalhado)
        {
            if (!detalhado)
            {
                var filmeResumido = await _filmeService.Value.BuscarResumidoPorFiltroAsync(filmeFiltroDTO);
                return await base.CreateResponse(filmeResumido, 200);
            }
            var filmeDetalhado = await _filmeService.Value.BuscarDetalhadoPorFiltroAsync(filmeFiltroDTO);
            return await base.CreateResponse(filmeDetalhado, 200);
        }

        [CustomAuthorize("ROLE", "BASICO")]
        [HttpPost("avaliar")]
        public async Task<ActionResult> Avaliar([FromBody]AvaliacaoDTO avaliacaoDTO)
        {
            await _filmeService.Value.AvaliarAsync(avaliacaoDTO.IdFilme, avaliacaoDTO.Nota);
            return await base.CreateResponse<object>(null, 200);
        }
    }
}
