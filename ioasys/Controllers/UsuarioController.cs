﻿using ioasys.Core.Configuracao;
using ioasys.Core.Configuracao.Autorizacao;
using ioasys.Core.Constantes;
using ioasys.Core.DTO;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using NetDevPack.Identity.Authorization;
using System;
using System.Threading.Tasks;

namespace ioasys.Controllers
{
    [Route(UrlApi.BaseUrl + "usuario")]
    public class UsuarioController : BaseController
    {
        private readonly IAutorizacaoIdentity _autorizacao;
        public UsuarioController(IAutorizacaoIdentity autorizacao, InjectorAplicacao injector)
           : base(injector)
        {
            _autorizacao = autorizacao;
        }

        [Authorize]
        [CustomAuthorize("ROLE", "ADMINISTRADOR")]
        [HttpPost("cadastrar")]
        public async Task<ActionResult> CadastrarUsuario([FromBody] UsuarioAplicacaoDTO usuarioDTO)
        {
            await _autorizacao.CadastrarAsync(base.Injector.Mapper.Map<UsuarioIdentity>(usuarioDTO), usuarioDTO.Role, usuarioDTO.Senha);
            return await base.CreateResponse<object>(null, 201);
        }

        [Authorize]
        [HttpPost("editar")]
        public async Task<ActionResult> EditarUsuario([FromBody] UsuarioAplicacaoEdicaoDTO usuarioDTO)
        {
            await _autorizacao.EditarAsync(base.Injector.Mapper.Map<UsuarioIdentity>(usuarioDTO));
            return await base.CreateResponse<object>(null, 204);
        }

        [Authorize]
        [CustomAuthorize("ROLE", "ADMINISTRADOR")]
        [HttpGet]
        public async Task<ActionResult> BuscarUsuarios()
        {
            var usuarios = await base.Injector.UsuarioAplicacao.RetornarUsuariosAtivos();
            return await base.CreateResponse(usuarios, 200);
        }

        [HttpPost("login")]
        public async Task<ActionResult> Login([FromBody] UsuarioLoginDTO usuarioDTO)
        {
            var resultadoLogin = await _autorizacao.LoginAsync(usuarioDTO);
            return await base.CreateResponse(resultadoLogin ? _autorizacao.GerarToken(usuarioDTO.Email) : null, 200);
        }

        [Authorize]
        [HttpPut("desativar/{id?}")]
        public async Task<ActionResult> DesativarUsuario([FromRoute] string id)
        {
            await _autorizacao.DesativarAsync(id ?? base.Injector.UsuarioAplicacao.RetornarIdUsuarioLogado());
            return await base.CreateResponse<object>(null, 204);
        }

        [Authorize]
        [CustomAuthorize("ROLE", "ADMINISTRADOR")]
        [HttpPut("ativar/{id}")]
        public async Task<ActionResult> AtivarUsuario([FromRoute] string id)
        {
            await _autorizacao.AtivarAsync(id);
            return await base.CreateResponse<object>(null, 204);
        }
    }
}
