﻿using ioasys.Core.Configuracao;
using NetDevPack.Identity.Authorization;

namespace ioasys.Controllers
{
    [CustomAuthorize("ATIVO", "1")]
    public class BaseAtivoController : BaseController
    {
        public BaseAtivoController(InjectorAplicacao injector) : base(injector)
        {
        }
    }
}
