﻿using Dominio.Service.Validadores.Base;
using ioasys.Core.Configuracao;
using ioasys.Core.DTO;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Threading.Tasks;

namespace ioasys.Controllers
{
    [ApiController]
    public abstract class BaseController : Controller
    {
        protected readonly InjectorAplicacao Injector;
        public BaseController(InjectorAplicacao injector)
        {
            Injector = injector;
        }
        public Task<ActionResult> CreateResponse<TResponse>(TResponse response, int statusCode)
        {
            if (Injector.Notificador.Valido())
                return Task.FromResult<ActionResult>(StatusCode(statusCode, CriarObjetoRetorno(response)));
            if (Injector.Notificador.RetornarNotificacoes(x => x.Mensagem.Contains(MensagensValidador.NotFound)).Any())
                return Task.FromResult<ActionResult>(NotFound(CriarObjetoRetorno(Injector.Notificador.RetornarNotificacoes())));
            return Task.FromResult<ActionResult>(BadRequest(CriarObjetoRetorno(Injector.Notificador.RetornarNotificacoes())));
        }

        protected ResponseDTO<TResponse> CriarObjetoRetorno<TResponse>(TResponse response) =>
            new ResponseDTO<TResponse> 
            { 
                Dados = response, 
            };
    }
}
