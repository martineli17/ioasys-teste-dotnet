﻿using Dominio.Contratos.Repositorios;
using Dominio.Entidades;
using Repositorio.Repositorios.Base;

namespace Repositorio.Repositorios
{
    public class AtorRepositorio : RepositorioBase<Ator>, IAtorRepositorio
    {
        public AtorRepositorio(InjectorRepositorio injector) : base(injector)
        {
        }
    }
}