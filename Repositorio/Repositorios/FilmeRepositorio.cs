﻿using Crosscuting.Extensoes;
using Dominio.Contratos.Repositorios;
using Dominio.DTO;
using Dominio.Entidades;
using Microsoft.EntityFrameworkCore;
using Repositorio.Repositorios.Base;
using System;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Repositorio.Repositorios
{
    public class FilmeRepositorio : RepositorioBase<Filme>, IFilmeRepositorio
    {
        public FilmeRepositorio(InjectorRepositorio injector) : base(injector)
        {
        }

        public Task<IQueryable<Filme>> BuscarPorFiltroAsync(FilmeFiltroDTO filtro, bool detalhado)
        {
            var filmes = Context.Filme.AsQueryable();
            if (detalhado) filmes = filmes.Include(x => x.Diretor).Include(x => x.Atores).ThenInclude(x => x.Ator).Include(x => x.Avaliacoes);
            else if (filtro.Diretor.HasValue()) filmes.Include(x => x.Diretor);
            filmes = filmes.Where(MontarFiltroExpression(filtro)).OrderBy(x => x.Nome).Skip(filtro.GetSkip()).Take(filtro.TamanhoPagina);
            return Task.FromResult(detalhado ? filmes : filmes.Select( x=> new Filme { Id = x.Id, Nome = x.Nome, IdadeRecomendada = x.IdadeRecomendada, Genero = x.Genero }));
        }

        #region Métodos Privados
        private Expression<Func<Filme, bool>> MontarFiltroExpression(FilmeFiltroDTO filtro)
        {
            return x => (!filtro.Id.HasValue || x.Id == filtro.Id.Value) &&
                                   (!filtro.Genero.HasValue || x.Genero == filtro.Genero.Value) &&
                                   (!filtro.Diretor.HasValue() || x.Diretor.Nome.ToLower().Contains(filtro.Diretor.ToLower())) &&
                                   (!filtro.Ator.HasValue() || x.Atores.Any(af => af.Ator.Nome.ToLower().Contains(filtro.Ator.ToLower()))) &&
                                   (!filtro.Nome.HasValue() || x.Nome.ToLower().Contains(filtro.Nome.ToLower()));
        }
           
        #endregion
    }
}