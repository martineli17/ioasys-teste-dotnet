﻿using Dominio.Contratos.Repositorios;
using Dominio.Entidades;
using Repositorio.Repositorios.Base;

namespace Repositorio.Repositorios
{
    public class DiretorRepositorio : RepositorioBase<Diretor>, IDiretorRepositorio
    {
        public DiretorRepositorio(InjectorRepositorio injector) : base(injector)
        {
        }
    }
}