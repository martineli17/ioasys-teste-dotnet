﻿using Crosscuting.Servicos.Notificadores;
using Dominio.Contratos.Repositorios.Base;
using Dominio.DTO;
using Dominio.Entidades.Base;
using Microsoft.EntityFrameworkCore;
using Repositorio.Contexto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Repositorio.Repositorios.Base
{
    public class RepositorioBase<TEntidade> : IDisposable, IRepositorioBase<TEntidade> where TEntidade : EntidadeBase
    {
        protected readonly Context Context;
        protected readonly INotificador Notificador;

        public RepositorioBase(InjectorRepositorio injector)
        {
            Context = injector.Context;
            Notificador = injector.Notificador;
        }

        public virtual async Task AdicionarAsync(TEntidade entidade)
        {
            await Context.Set<TEntidade>().AddAsync(entidade);
        }

        public virtual async Task AdicionarAsync(IEnumerable<TEntidade> entidades)
        {
            await Context.Set<TEntidade>().AddRangeAsync(entidades);
        }

        public virtual Task<IQueryable<TEntidade>> BuscarAsync(Expression<Func<TEntidade, bool>> filtro = null, PaginacaoDTO paginacao = null, params string[] includes)
        {
            var query = Context.Set<TEntidade>().AsQueryable();
            if (filtro != null)
                query = query.Where(filtro);
            if (paginacao != null)
                query.Skip(paginacao.GetSkip()).Take(paginacao.TamanhoPagina);
            if (includes != null) query = includes.Aggregate(query, (current, includeProperty) => current.Include(includeProperty));
            return Task.FromResult(query.AsNoTracking());
        }

        public virtual async Task<TEntidade> BuscarPorId(Guid id)
        {
            var entidade = await Context.Set<TEntidade>().FindAsync(id);
            if (entidade != null)
                Context.Entry(entidade).State = EntityState.Detached;
            return entidade;
        }

        public virtual async Task RemoverAsync(Guid id)
        {
            var entidade = await BuscarPorId(id);
            if (entidade == null)
            {
                Notificador.Inserir("Registro não encontrado.");
                return;
            }
            Context.Set<TEntidade>().Remove(entidade);
        }

        public virtual async Task RemoverAsync(IEnumerable<Guid> id)
        {
            await Task.Yield();
            var entidades = Context.Set<TEntidade>().Where(x => id.Contains(x.Id));
            Context.Set<TEntidade>().RemoveRange(entidades);
        }

        public virtual async Task AtualizarAsync(TEntidade entidade)
        {
            await Task.Yield();
            Context.Set<TEntidade>().Update(entidade);
        }

        public virtual async Task AtualizarAsync(IEnumerable<TEntidade> entidades)
        {
            await Task.Yield();
            Context.Set<TEntidade>().UpdateRange(entidades);
        }

        public virtual async Task<bool> ExisteAsync(Expression<Func<TEntidade, bool>> filtro)
        {
            return await Context.Set<TEntidade>().AnyAsync(filtro);
        }

        public void Dispose()
        {
            Context.Dispose();
            GC.SuppressFinalize(this);
        }
    }
}
