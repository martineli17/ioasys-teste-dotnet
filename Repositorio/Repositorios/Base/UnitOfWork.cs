﻿using Crosscuting.Servicos.Notificadores;
using Dominio.Contratos.Base;
using System;
using System.Threading.Tasks;

namespace Repositorio.Repositorios.Base
{
    public class UnitOfWork : IUnitOfWork, IDisposable
    {
        private readonly InjectorRepositorio _injector;
        public UnitOfWork(InjectorRepositorio injector)
        {
            _injector = injector;
        }
        public async Task<bool> CommitAsync()
        {
            try
            {
                var changes = await _injector.Context.SaveChangesAsync();
                return changes > 0;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                _injector.Notificador.Inserir("Ocorreu um erro ao processar a operação.", EnumTipoNotificacao.Error);
                return false;
            }
        }

        public void Dispose()
        {
            _injector.Dispose();
            GC.SuppressFinalize(this);
        }
    }
}
