﻿using Crosscuting.Servicos.Notificadores;
using Repositorio.Contexto;
using System;

namespace Repositorio.Repositorios.Base
{
    public class InjectorRepositorio : IDisposable
    {
        public readonly Context Context;
        public readonly INotificador Notificador;

        public InjectorRepositorio(Context context, INotificador notificador)
        {
            Context = context;
            Notificador = notificador;
        }

        public void Dispose()
        {
            Context.Dispose();
            GC.SuppressFinalize(this);
        }
    }
}
