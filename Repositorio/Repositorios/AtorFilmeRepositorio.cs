﻿using Dominio.Contratos.Repositorios;
using Dominio.Entidades;
using Repositorio.Repositorios.Base;

namespace Repositorio.Repositorios
{
    public class AtorFilmeRepositorio : RepositorioBase<AtorFilme>, IAtorFilmeRepositorio
    {
        public AtorFilmeRepositorio(InjectorRepositorio injector) : base(injector)
        {
        }
    }
}
