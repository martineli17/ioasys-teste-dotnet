﻿using Dominio.Contratos.Repositorios;
using Dominio.Entidades;
using Repositorio.Repositorios.Base;

namespace Repositorio.Repositorios
{
    public class AvaliacaoRepositorio : RepositorioBase<Avaliacao>, IAvaliacaoRepositorio
    {
        public AvaliacaoRepositorio(InjectorRepositorio injector) : base(injector)
        {
        }
    }
}