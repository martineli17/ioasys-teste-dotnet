﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Repositorio.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "ATOR",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    DATA_CRIACAO = table.Column<DateTime>(type: "DATETIME", nullable: false),
                    NOME = table.Column<string>(type: "VARCHAR(50)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ATOR", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "DIRETOR",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    DATA_CRIACAO = table.Column<DateTime>(type: "DATETIME", nullable: false),
                    NOME = table.Column<string>(type: "VARCHAR(50)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DIRETOR", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "FILME",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    ID_USUARIO_CADASTRO = table.Column<string>(type: "VARCHAR(50)", nullable: false),
                    ID_DIRETOR = table.Column<Guid>(type: "UNIQUEIDENTIFIER", nullable: true),
                    NOME = table.Column<string>(type: "VARCHAR(50)", nullable: false),
                    TITULO = table.Column<string>(type: "VARCHAR(25)", nullable: false),
                    DESCRICAO = table.Column<string>(type: "VARCHAR(500)", nullable: false),
                    DATA_LANCAMENTO = table.Column<DateTime>(type: "DATETIME", nullable: true),
                    IDADE_RECOMENDADA = table.Column<int>(type: "INT", nullable: false),
                    GENERO = table.Column<string>(type: "VARCHAR(15)", nullable: false),
                    DATA_CRIACAO = table.Column<DateTime>(type: "DATETIME", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FILME", x => x.Id);
                    table.ForeignKey(
                        name: "FK_FILME_DIRETOR_ID_DIRETOR",
                        column: x => x.ID_DIRETOR,
                        principalTable: "DIRETOR",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ATOR_FILME",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    ID_FILME = table.Column<Guid>(type: "UNIQUEIDENTIFIER", nullable: false),
                    ID_ATOR = table.Column<Guid>(type: "UNIQUEIDENTIFIER", nullable: false),
                    DATA_CRIACAO = table.Column<DateTime>(type: "DATETIME", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ATOR_FILME", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ATOR_FILME_ATOR_ID_ATOR",
                        column: x => x.ID_ATOR,
                        principalTable: "ATOR",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ATOR_FILME_FILME_ID_FILME",
                        column: x => x.ID_FILME,
                        principalTable: "FILME",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AVALICACAO",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    IdFilme = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    NOTA = table.Column<int>(type: "INT", nullable: false),
                    DATA_CRIACAO = table.Column<DateTime>(type: "DATETIME", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AVALICACAO", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AVALICACAO_FILME_IdFilme",
                        column: x => x.IdFilme,
                        principalTable: "FILME",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ATOR_NOME",
                table: "ATOR",
                column: "NOME");

            migrationBuilder.CreateIndex(
                name: "IX_ATOR_FILME_ID_ATOR",
                table: "ATOR_FILME",
                column: "ID_ATOR");

            migrationBuilder.CreateIndex(
                name: "IX_ATOR_FILME_ID_FILME",
                table: "ATOR_FILME",
                column: "ID_FILME");

            migrationBuilder.CreateIndex(
                name: "IX_AVALICACAO_IdFilme",
                table: "AVALICACAO",
                column: "IdFilme");

            migrationBuilder.CreateIndex(
                name: "IX_DIRETOR_NOME",
                table: "DIRETOR",
                column: "NOME");

            migrationBuilder.CreateIndex(
                name: "IX_FILME_GENERO",
                table: "FILME",
                column: "GENERO");

            migrationBuilder.CreateIndex(
                name: "IX_FILME_ID_DIRETOR",
                table: "FILME",
                column: "ID_DIRETOR");

            migrationBuilder.CreateIndex(
                name: "IX_FILME_NOME",
                table: "FILME",
                column: "NOME");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ATOR_FILME");

            migrationBuilder.DropTable(
                name: "AVALICACAO");

            migrationBuilder.DropTable(
                name: "ATOR");

            migrationBuilder.DropTable(
                name: "FILME");

            migrationBuilder.DropTable(
                name: "DIRETOR");
        }
    }
}
