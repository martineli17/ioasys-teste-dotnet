﻿using Crosscuting.Extensoes;
using Dominio.Entidades;
using Dominio.TiposValores;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Repositorio.Mappings
{
    class FilmeMapping : IEntityTypeConfiguration<Filme>
    {
        public void Configure(EntityTypeBuilder<Filme> builder)
        {
            builder.HasKey(x => x.Id);
            builder.ToTable("FILME");
            builder.HasIndex(x => x.Nome);
            builder.HasIndex(x => x.Genero);
            builder.Property(x => x.DataCriacao).HasColumnName("DATA_CRIACAO").HasColumnType("DATETIME").IsRequired();
            builder.Property(x => x.Nome).HasColumnName("NOME").HasColumnType("VARCHAR(50)").IsRequired();
            builder.Property(x => x.IdUsuarioCadastro).HasColumnName("ID_USUARIO_CADASTRO").HasColumnType("VARCHAR(50)").IsRequired();
            builder.Property(x => x.IdDiretor).HasColumnName("ID_DIRETOR").HasColumnType("UNIQUEIDENTIFIER").IsRequired(false);
            builder.Property(x => x.Titulo).HasColumnName("TITULO").HasColumnType("VARCHAR(25)").IsRequired();
            builder.Property(x => x.Descricao).HasColumnName("DESCRICAO").HasColumnType("VARCHAR(500)").IsRequired();
            builder.Property(x => x.DataLancamento).HasColumnName("DATA_LANCAMENTO").HasColumnType("DATETIME").IsRequired(false);
            builder.Property(x => x.IdadeRecomendada).HasColumnName("IDADE_RECOMENDADA").HasColumnType("INT").IsRequired();
            builder.Property(x => x.Genero).HasColumnName("GENERO").HasColumnType("VARCHAR(15)").IsRequired()
                .HasConversion(valueToDataBase => valueToDataBase.ToString(), valueFromDataBase => valueFromDataBase.ToEnum<EnumGeneroFilme>());
            builder.HasOne(x => x.Diretor).WithMany(x => x.Filmes).HasForeignKey(x => x.IdDiretor);
            builder.HasMany(x => x.Atores).WithOne(x => x.Filme).HasForeignKey(x => x.IdFilme).OnDelete(DeleteBehavior.Cascade);
            builder.HasMany(x => x.Avaliacoes).WithOne(x => x.Filme).HasForeignKey(x => x.IdFilme).OnDelete(DeleteBehavior.Cascade);
        }
    }
}
