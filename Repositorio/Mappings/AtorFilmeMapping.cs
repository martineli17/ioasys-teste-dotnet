﻿using Dominio.Entidades;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Repositorio.Mappings
{
    public class AtorFilmeMapping : IEntityTypeConfiguration<AtorFilme>
    {
        public void Configure(EntityTypeBuilder<AtorFilme> builder)
        {
            builder.HasKey(x => x.Id);
            builder.ToTable("ATOR_FILME");
            builder.Property(x => x.DataCriacao).HasColumnName("DATA_CRIACAO").HasColumnType("DATETIME").IsRequired();
            builder.Property(x => x.IdAtor).HasColumnName("ID_ATOR").HasColumnType("UNIQUEIDENTIFIER").IsRequired();
            builder.Property(x => x.IdFilme).HasColumnName("ID_FILME").HasColumnType("UNIQUEIDENTIFIER").IsRequired();
            builder.HasOne(x => x.Filme).WithMany(x => x.Atores).HasForeignKey(x => x.IdFilme);
            builder.HasOne(x => x.Ator).WithMany(x => x.Filmes).HasForeignKey(x => x.IdAtor);
        }
    }
}
