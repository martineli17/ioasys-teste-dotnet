﻿using Dominio.Entidades;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Repositorio.Mappings
{
    public class DiretorMapping : IEntityTypeConfiguration<Diretor>
    {
        public void Configure(EntityTypeBuilder<Diretor> builder)
        {
            builder.HasKey(x => x.Id);
            builder.ToTable("DIRETOR");
            builder.HasIndex(x => x.Nome);
            builder.Property(x => x.DataCriacao).HasColumnName("DATA_CRIACAO").HasColumnType("DATETIME").IsRequired();
            builder.Property(x => x.Nome).HasColumnName("NOME").HasColumnType("VARCHAR(50)").IsRequired();
            builder.HasMany(x => x.Filmes).WithOne(x => x.Diretor).HasForeignKey(x => x.IdDiretor).OnDelete(DeleteBehavior.ClientSetNull);
        }
    }
}
