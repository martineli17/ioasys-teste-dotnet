﻿using Dominio.Entidades;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Repositorio.Mappings
{
    public class AtorMapping : IEntityTypeConfiguration<Ator>
    {
        public void Configure(EntityTypeBuilder<Ator> builder)
        {
            builder.HasKey(x => x.Id);
            builder.ToTable("ATOR");
            builder.HasIndex(x => x.Nome);
            builder.Property(x => x.DataCriacao).HasColumnName("DATA_CRIACAO").HasColumnType("DATETIME").IsRequired();
            builder.Property(x => x.Nome).HasColumnName("NOME").HasColumnType("VARCHAR(50)").IsRequired();
            builder.HasMany(x => x.Filmes).WithOne(x => x.Ator).HasForeignKey(x => x.IdAtor).OnDelete(DeleteBehavior.Cascade);
        }
    }
}
