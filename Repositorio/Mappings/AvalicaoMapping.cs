﻿using Dominio.Entidades;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Repositorio.Mappings
{
    public class AvalicaoMapping : IEntityTypeConfiguration<Avaliacao>
    {
        public void Configure(EntityTypeBuilder<Avaliacao> builder)
        {
            builder.HasKey(x => x.Id);
            builder.ToTable("AVALICACAO");
            builder.HasIndex(x => x.IdFilme);
            builder.Property(x => x.DataCriacao).HasColumnName("DATA_CRIACAO").HasColumnType("DATETIME").IsRequired();
            builder.Property(x => x.Nota).HasColumnName("NOTA").HasColumnType("INT").IsRequired();
            builder.HasOne(x => x.Filme).WithMany(x => x.Avaliacoes).HasForeignKey(x => x.IdFilme);
        }
    }
}
