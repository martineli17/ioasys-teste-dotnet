﻿using Dominio.Entidades;
using Microsoft.EntityFrameworkCore;
using System;

namespace Repositorio.Contexto
{
    public class Context : DbContext, IDisposable
    {
        public Context(DbContextOptions<Context> options) : base(options)
        {

        }

        public DbSet<Ator> Ator { get; set; }
        public DbSet<AtorFilme> AtorFilme { get; set; }
        public DbSet<Avaliacao> Avaliacao { get; set; }
        public DbSet<Filme> Filme { get; set; }
        public DbSet<Diretor> Diretor { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.ApplyConfigurationsFromAssembly(GetType().Assembly);
            base.OnModelCreating(builder);
        }

        public override void Dispose()
        {
            GC.SuppressFinalize(this);
        }
    }
}
