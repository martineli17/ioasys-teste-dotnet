﻿using Crosscuting.Extensoes;
using Dominio.Entidades.Base;
using Dominio.TiposValores;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Dominio.Entidades
{
    public class Filme : EntidadeBase
    {
        public Guid IdUsuarioCadastro { get; set; }
        public Guid? IdDiretor { get; set; }
        public string Nome { get; set; }
        public string Titulo { get; set; }
        public string Descricao { get; set; }
        public DateTime? DataLancamento { get; set; }
        public int IdadeRecomendada { get; set; }
        public EnumGeneroFilme Genero { get; set; }
        public IEnumerable<AtorFilme> Atores { get; set; }
        public IEnumerable<Avaliacao> Avaliacoes { get; set; }
        public Diretor Diretor { get; set; }
        public Filme()
        {

        }
        public int ContarTotalDeVotos() => Avaliacoes.HasValue() ? Avaliacoes.Count() : 0;
        public int? CalcularTotalDeNotas() => Avaliacoes.HasValue() ? Avaliacoes.Sum(x => x.ValorNota()) : (int?)null;
        public double? CalcularMediaAvalicacao() => Avaliacoes.HasValue() ? Math.Round((Avaliacoes.Sum(x => (double)x.ValorNota()) / Avaliacoes.Count()),2) : (double?)null;
    }
}
