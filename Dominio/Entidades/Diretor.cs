﻿using Dominio.Entidades.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace Dominio.Entidades
{
    public class Diretor : Pessoa
    {
        public IEnumerable<Filme> Filmes { get; set; }
        public Diretor()
        {

        }
    }
}
