﻿using System;

namespace Dominio.Entidades.Base
{
    public class EntidadeBase
    {
        public Guid Id { get; set; }
        public DateTime DataCriacao { get; set; }

       
        public EntidadeBase()
        {
            Id = Guid.NewGuid();
            DataCriacao = DateTime.Now;
        }
    }
}
