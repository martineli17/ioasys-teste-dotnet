﻿using Dominio.Entidades.Base;
using System;

namespace Dominio.Entidades
{
    public class AtorFilme : EntidadeBase
    {
        public Guid IdFilme { get; set; }
        public Guid IdAtor { get; set; }
        public Filme Filme { get; set; }
        public Ator Ator { get; set; }


        public AtorFilme()
        {

        }
    }
}
