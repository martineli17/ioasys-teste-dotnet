﻿using Dominio.Entidades.Base;
using Dominio.TiposValores;
using System;

namespace Dominio.Entidades
{
    public class Avaliacao : EntidadeBase
    {
        public Guid IdFilme { get; set; }
        public Filme Filme { get; set; }
        public EnumAvaliacaoFilme Nota { get; set; }

        public Avaliacao()
        {

        }
        public int ValorNota() => Nota.GetHashCode();
    }
}
