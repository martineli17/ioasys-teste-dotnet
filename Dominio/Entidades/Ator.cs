﻿using Dominio.Entidades.Base;
using System;
using System.Collections.Generic;

namespace Dominio.Entidades
{
    public class Ator : Pessoa
    {
        public IEnumerable<AtorFilme> Filmes { get; set; }
        public Ator()
        {

        }
    }
}
