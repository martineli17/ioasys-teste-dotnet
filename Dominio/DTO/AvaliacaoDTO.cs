﻿using Dominio.TiposValores;
using System;

namespace Dominio.DTO
{
    public class AvaliacaoDTO
    {
        public Guid IdFilme { get; set; }
        public EnumAvaliacaoFilme Nota { get; set; }
    }
}
