﻿using Dominio.TiposValores;
using System;
using System.Collections.Generic;

namespace Dominio.DTO
{
    public class FilmeDTO
    {
        public string IdUsuarioCadastro { get; set; }
        public string Nome { get; set; }
        public string Titulo { get; set; }
        public string Descricao { get; set; }
        public DateTime? DataLancamento { get; set; }
        public int IdadeRecomendada { get; set; }
        public EnumGeneroFilme Genero { get; set; }
        public IEnumerable<string> Atores { get; set; }
        public string Diretor { get; set; }
    }

    public class FilmeFiltroDTO : PaginacaoDTO
    {
        public Guid? Id { get; set; }
        public string Nome { get; set; }
        public string Diretor { get; set; }
        public string Ator { get; set; }
        public EnumGeneroFilme? Genero { get; set; }
    }

    public class FilmeResumidoDTO
    {
        public Guid Id { get; set; }
        public string Nome { get; set; }
        public int IdadeRecomendada { get; set; }
        public EnumGeneroFilme? Genero { get; set; }
    }
    public class FilmeDetalhadoDTO : FilmeDTO
    {
        public Guid Id { get; set; }
        public double? Avaliacao { get; set; }
        public int TotalDeVotos { get; set; }
    }
}
