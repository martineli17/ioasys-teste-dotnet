﻿namespace Dominio.DTO
{
    public class PaginacaoDTO
    {
        public int TamanhoPagina { get; set; } = 50;
        public int NumeroPagina { get; set; } = 1;
        public int GetSkip() => ((NumeroPagina <= 0 ? 1 : NumeroPagina)- 1) * TamanhoPagina;
    }
}
