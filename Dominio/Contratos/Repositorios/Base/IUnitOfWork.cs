﻿using System.Threading.Tasks;

namespace Dominio.Contratos.Base
{
    public interface IUnitOfWork
    {
        Task<bool> CommitAsync();
    }
}
