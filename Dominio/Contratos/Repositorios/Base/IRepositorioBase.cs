﻿using Dominio.DTO;
using Dominio.Entidades.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Dominio.Contratos.Repositorios.Base
{
    public interface IRepositorioBase<TEntidade> where TEntidade : EntidadeBase
    {
        Task AdicionarAsync(TEntidade entidade);
        Task AdicionarAsync(IEnumerable<TEntidade> entidade);
        Task RemoverAsync(Guid id);
        Task RemoverAsync(IEnumerable<Guid> id);
        Task AtualizarAsync(TEntidade entidade);
        Task AtualizarAsync(IEnumerable<TEntidade> entidade);
        Task<IQueryable<TEntidade>> BuscarAsync(Expression<Func<TEntidade, bool>> query = null, PaginacaoDTO paginacao = null, params string[] includes);
        Task<TEntidade> BuscarPorId(Guid id);
        Task<bool> ExisteAsync(Expression<Func<TEntidade, bool>> filtro);
    }
}
