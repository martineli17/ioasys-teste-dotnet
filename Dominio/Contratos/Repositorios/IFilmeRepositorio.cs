﻿using Dominio.Contratos.Repositorios.Base;
using Dominio.DTO;
using Dominio.Entidades;
using System.Linq;
using System.Threading.Tasks;

namespace Dominio.Contratos.Repositorios
{
    public interface IFilmeRepositorio : IRepositorioBase<Filme>
    {
        Task<IQueryable<Filme>> BuscarPorFiltroAsync(FilmeFiltroDTO filtro, bool detalhado);
    }
}
