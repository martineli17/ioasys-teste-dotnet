﻿using Dominio.Contratos.Servicos.Base;
using Dominio.Entidades;
using System;
using System.Threading.Tasks;

namespace Dominio.Contratos.Servicos
{
    public interface IDiretorService : IServiceBase
    {
        Task<Guid?> AdicionarAsync(Diretor diretor);
    }
}
