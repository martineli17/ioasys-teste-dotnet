﻿namespace Dominio.Contratos.Servicos.Base
{
    public interface IServiceBase
    {
        void DesabilitarAutoCommit();
        void HabilitarAutoCommit();
    }
}
