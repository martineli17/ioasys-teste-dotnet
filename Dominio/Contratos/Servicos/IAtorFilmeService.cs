﻿using Dominio.Contratos.Servicos.Base;
using Dominio.Entidades;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Dominio.Contratos.Servicos
{
    public interface IAtorFilmeService : IServiceBase
    {
        Task<bool> AdicionarAsync(List<AtorFilme> atoresFilme);
    }
}
