﻿using Dominio.DTO;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Dominio.Contratos.Servicos
{
    public interface IUsuarioAplicacaoService
    {
        string RetornarIdUsuarioLogado();
        Task<List<UsuarioDTO>> RetornarUsuariosAtivos();
    }
}
