﻿using Dominio.DTO;
using System.Threading.Tasks;

namespace Dominio.Contratos.Servicos
{
    public interface IFilmeFacadeService
    {
        Task<bool> AdicionarAsync(FilmeDTO filmeDTO);
    }
}
