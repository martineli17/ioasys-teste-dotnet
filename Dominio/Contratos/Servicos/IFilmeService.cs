﻿using Dominio.Contratos.Servicos.Base;
using Dominio.DTO;
using Dominio.Entidades;
using Dominio.TiposValores;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Dominio.Contratos.Servicos
{
    public interface IFilmeService : IServiceBase
    {
        Task AdicionarAsync(Filme filmeDTO);
        Task AvaliarAsync(Guid idFilme, EnumAvaliacaoFilme nota);
        Task<IEnumerable<FilmeResumidoDTO>> BuscarResumidoPorFiltroAsync(FilmeFiltroDTO filtro);
        Task<IEnumerable<FilmeDetalhadoDTO>> BuscarDetalhadoPorFiltroAsync(FilmeFiltroDTO filtro);
    }
}
