﻿using Dominio.Contratos.Servicos.Base;
using Dominio.Entidades;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Dominio.Contratos.Servicos
{
    public interface IAtorService : IServiceBase
    {
        Task<IEnumerable<Guid>> AdicionarAsync(List<Ator> atores);
    }
}
