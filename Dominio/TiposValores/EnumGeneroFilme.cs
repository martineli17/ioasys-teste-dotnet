﻿using System.ComponentModel.DataAnnotations;

namespace Dominio.TiposValores
{
    public enum EnumGeneroFilme
    {
        [Display(Name = "Ação")]
        Acao = 1,
        [Display(Name = "Aventura")]
        Aventura = 2,
        [Display(Name = "Ficção")]
        Ficcao = 3,
        [Display(Name = "Romance")]
        Romance = 4,
        [Display(Name = "Comédia")]
        Comedia = 5,
        [Display(Name = "Documentário")]
        Documentario = 6,
        [Display(Name = "Terror")]
        Terror = 7,
        [Display(Name = "Suspense")]
        Suspense = 8,
        [Display(Name = "Musical/Dança")]
        MusicalDanca = 9,
    }
}
