﻿using System.ComponentModel.DataAnnotations;

namespace Dominio.TiposValores
{
    public enum EnumAvaliacaoFilme
    {
        [Display(Name = "Ruim")]
        Ruim = 0,
        [Display(Name = "Médio")]
        Medio = 1,
        [Display(Name = "Bom")]
        Bom = 2,
        [Display(Name = "Muito bom")]
        MuitoBom = 3,
        [Display(Name = "Excelente")]
        Excelente = 4
    }
}
