﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Crosscuting.Extensoes
{
    public static class IEnumerableExtension
    {
        public static bool HasValue<TValue>(this IEnumerable<TValue> value) => value != null && value.Any();
    }
}
