﻿using System;

namespace Crosscuting.Extensoes
{
    public static class StringExtension
    {
        public static T ToEnum<T>(this string value) => (T)Enum.Parse(typeof(T), value, true);
        public static bool HasValue(this string value) => !(string.IsNullOrEmpty(value) || string.IsNullOrWhiteSpace(value));
    }
}
