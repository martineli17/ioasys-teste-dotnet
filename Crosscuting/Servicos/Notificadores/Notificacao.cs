﻿namespace Crosscuting.Servicos.Notificadores
{
    public class Notificacao
    {
        public string Mensagem { get; private set; }
        public EnumTipoNotificacao Tipo { get; private set; }

        public Notificacao(string mensagem, EnumTipoNotificacao tipo)
        {
            Mensagem = mensagem;
            Tipo = tipo;
        }

    }
}
