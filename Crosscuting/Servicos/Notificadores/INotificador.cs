﻿using System;
using System.Collections.Generic;

namespace Crosscuting.Servicos.Notificadores
{
    public interface INotificador
    {
        void Inserir(string mensagem, EnumTipoNotificacao tipo = EnumTipoNotificacao.Warning);
        void Inserir(IEnumerable<string> mensagens, EnumTipoNotificacao tipo = EnumTipoNotificacao.Warning);
        void Limpar();
        bool Valido();
        List<Notificacao> RetornarNotificacoes(Func<Notificacao, bool> filtro = null);
    }
}
