﻿using Crosscuting.Extensoes;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Crosscuting.Servicos.Notificadores
{
    public class Notificador : INotificador
    {
        private List<Notificacao> _notificacoes;
        public Notificador()
        {
            _notificacoes = new List<Notificacao>();
        }
        public void Inserir(string mensagem, EnumTipoNotificacao tipo = EnumTipoNotificacao.Warning)
            => _notificacoes.Add(new Notificacao(mensagem, tipo));
        public void Inserir(IEnumerable<string> mensagens, EnumTipoNotificacao tipo = EnumTipoNotificacao.Warning)
           => _notificacoes.AddRange(mensagens.Select(x => new Notificacao(x, tipo)));
        public void Limpar() => _notificacoes.Clear();
        public bool Valido() => !_notificacoes.HasValue();
        public List<Notificacao> RetornarNotificacoes(Func<Notificacao, bool> filtro = null) => 
            filtro is null ? _notificacoes :  _notificacoes.Where(filtro).ToList();
    }
}
