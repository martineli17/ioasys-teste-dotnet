# SDK
Aplicação implementada através da versão 3.1 do SDK do *.NET Core*.

# Configurações
## - É necessário realizar a execução das Migrations, visto que o código foi realizado seguindo o modelo Code First.
É preciso executar tais comandos no projeto de ***'Repositorio'*** e no projeto ***'Ioasys'***.
>###### ioasys: update-database -Context ContextIdentity
>###### Repositorio: update-database -Context Context

## - Antes de iniciar a aplicação, *precisa cadastrar um usuário administrador* no banco de dados para acessar a aplicação pela primeira vez.
Abaixo, segue um exemplo de script para tal etapa:
#
>***senha: @adm2021IOASYS***
#
- *INSERT INTO AspNetUsers*  
  *VALUES('10e7efc1-5114-4871-bc0c-8b2bc3560720', 1, 'administrador', 'administrador@email.com', 'ADMINISTRADOR@EMAIL.COM',*
  *'administrador@email.com', 'ADMINISTRADOR@EMAIL.COM', 0, 'AQAAAAEAACcQAAAAEGRvqorifvvXL6/ZHe3O5MbtT5m5YSxwWEZCqkKZ/tFLNuuwvTNJzBurvdsACYWVRw==',*
  *'O7QAXSTGZTBXRFP2HZTBWLTW4G3RYCXC', '7473aafd-a79b-4e5e-a6d0-0a8f3afdd983',  null, 0, 0, null,1, 0)*

- *INSERT INTO AspNetUserClaims VALUES('10e7efc1-5114-4871-bc0c-8b2bc3560720', 'ATIVO', '1')*
- *INSERT INTO AspNetUserClaims VALUES('10e7efc1-5114-4871-bc0c-8b2bc3560720', 'ROLE', 'ADMINISTRADOR')*
- *INSERT INTO AspNetUserClaims VALUES('10e7efc1-5114-4871-bc0c-8b2bc3560720', 'ID', '10e7efc1-5114-4871-bc0c-8b2bc3560720')*
#
## - Necessário também inserir a conexão com o seu banco de dados e as configurações em relação a autenticação/autorização.
Ambos, feitos no appSettings (ou no 'User Secrets' da sua aplicacao). Abaixo, segue um exemplo para tal etapa:

- *"ConnectionStrings": {*
      "SQLSERVER": "Server=INSIRA_AQUI_SEU_SERVIDOR;Database=ioasys;User Id=INSIRA_AQUI_SEU_USUARIO;Password=INSIRA_AQUI_SUA_SENHA;"
     },
- *"AppSettings": {
       "Expiration": 1,
       "Issuer": "MyApplication",
       "Audience": "https://localhost",
       "SecretKey": "INSIRA_AQUI_SEU_SECRET"
    }*