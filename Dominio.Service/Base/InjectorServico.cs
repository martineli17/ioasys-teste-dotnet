﻿using AutoMapper;
using Crosscuting.Servicos.Notificadores;
using Dominio.Contratos.Base;
using Dominio.Service.Validadores.Base;

namespace Dominio.Service.Base
{
    public class InjectorServico
    {
        public readonly IUnitOfWork UnitOfWork;
        public readonly IValidador Validador;
        public readonly INotificador Notificador;
        public readonly IMapper Mapper;

        public InjectorServico(IUnitOfWork unitOfWork, IMapper mapper, IValidador validador, INotificador notificador)
        {
            UnitOfWork = unitOfWork;
            Validador = validador;
            Notificador = notificador;
            Mapper = mapper;
        }
    }
}
