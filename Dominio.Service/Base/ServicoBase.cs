﻿using System;
using System.Threading.Tasks;

namespace Dominio.Service.Base
{
    public abstract class ServicoBase
    {
        protected readonly InjectorServico Injector;
        protected bool AutoCommit = true;
        public ServicoBase(InjectorServico injector)
        {
            Injector = injector;
        }
        public void DesabilitarAutoCommit() => AutoCommit = false;
        public void HabilitarAutoCommit() => AutoCommit = true;
        protected Task<bool> CommitAsync() => AutoCommit ? Injector.UnitOfWork.CommitAsync() : Task.FromResult(true);
    }
}
