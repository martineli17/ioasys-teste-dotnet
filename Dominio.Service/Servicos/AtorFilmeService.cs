﻿using Crosscuting.Extensoes;
using Dominio.Contratos.Repositorios;
using Dominio.Contratos.Servicos;
using Dominio.Entidades;
using Dominio.Service.Base;
using Dominio.Service.Validadores.Base;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dominio.Service.Servicos
{
    public class AtorFilmeService : ServicoBase, IAtorFilmeService
    {
        private readonly IAtorFilmeRepositorio _repositorio;
        public AtorFilmeService(InjectorServico injector, IAtorFilmeRepositorio repositorio) : base(injector)
        {
            _repositorio = repositorio;
        }

        public async Task<bool> AdicionarAsync(List<AtorFilme> atoresFilme)
        {
            var idsAtores = atoresFilme.Select(x => x.IdAtor);
            var idsFilmes = atoresFilme.Select(x => x.IdFilme);
            var atoresFilmesExistentes = (await _repositorio.BuscarAsync(x => idsAtores.Contains(x.IdAtor) && idsAtores.Contains(x.IdFilme))).ToList();
            if (atoresFilmesExistentes.HasValue() && atoresFilmesExistentes.TrueForAll(x => atoresFilme.Contains(x)))
                return true;
            await _repositorio.AdicionarAsync(atoresFilme.Where(x => !atoresFilmesExistentes.Contains(x)));
            return await base.CommitAsync();
        }
    }
}
