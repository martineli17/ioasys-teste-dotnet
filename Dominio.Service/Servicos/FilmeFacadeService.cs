﻿using Crosscuting.Extensoes;
using Dominio.Contratos.Servicos;
using Dominio.DTO;
using Dominio.Entidades;
using Dominio.Service.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dominio.Service.Servicos
{
    public class FilmeFacadeService : ServicoBase, IFilmeFacadeService
    {
        private readonly Lazy<IFilmeService> _filmeService;
        private readonly Lazy<IDiretorService> _diretorService;
        private readonly Lazy<IAtorFilmeService> _atorFilmeService;
        private readonly Lazy<IAtorService> _atorService;

        public FilmeFacadeService(InjectorServico injector,
            Lazy<IFilmeService> filmeService, Lazy<IDiretorService> diretorService,
            Lazy<IAtorFilmeService> atorFilmeService, Lazy<IAtorService> atorService) : base(injector)
        {
            _filmeService = filmeService;
            _diretorService = diretorService;
            _atorFilmeService = atorFilmeService;
            _atorService = atorService;
        }

        public async Task<bool> AdicionarAsync(FilmeDTO filmeDTO)
        {
            var idDiretor = await AddDiretorAsync(filmeDTO.Diretor);
            var atores = await AddAtoresAsync(filmeDTO.Atores);
            var filmeId = await AddFilmeAsync(filmeDTO, idDiretor);
            if (atores.HasValue()) await AddAtorFilmeAsync(atores, filmeId.Value);
            if (!Injector.Notificador.Valido()) return false;
            return await base.CommitAsync();
        }

        #region Métodos Privados
        private async Task<Guid?> AddFilmeAsync(FilmeDTO filmeDTO, Guid? idDiretor)
        {
            var filmeService = _filmeService.Value;
            var filme = base.Injector.Mapper.Map<Filme>(filmeDTO);
            filme.IdDiretor = idDiretor;
            filmeService.DesabilitarAutoCommit();
            await filmeService.AdicionarAsync(filme);
            return filme.Id;
        }
        private async Task<Guid?> AddDiretorAsync(string diretorDTO)
        {
            var diretorService = _diretorService.Value;
            diretorService.DesabilitarAutoCommit();
            if (diretorDTO.HasValue())
            {
                var diretor = new Diretor() { Nome = diretorDTO };
                return await diretorService.AdicionarAsync(diretor);
            }
            return (Guid?)null;
        }
        private async Task<IEnumerable<Guid>> AddAtoresAsync(IEnumerable<string> atoresDTO)
        {
            if (!atoresDTO.HasValue()) return null;
            var autorService = _atorService.Value;
            autorService.DesabilitarAutoCommit();
            var atores = atoresDTO.Select(x => new Ator() { Nome = x }).ToList();
            return (await autorService.AdicionarAsync(atores)).ToList();
        }
        private async Task AddAtorFilmeAsync(IEnumerable<Guid> atoresId, Guid filmeId)
        {
            var autorFilmeService = _atorFilmeService.Value;
            autorFilmeService.DesabilitarAutoCommit();
            var atoresFilmes = atoresId.Select(x => new AtorFilme() { IdAtor = x, IdFilme = filmeId }).ToList();
            await autorFilmeService.AdicionarAsync(atoresFilmes);
        }
        #endregion
    }
}
