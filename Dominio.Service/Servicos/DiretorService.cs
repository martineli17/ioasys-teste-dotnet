﻿using Dominio.Contratos.Repositorios;
using Dominio.Contratos.Servicos;
using Dominio.Entidades;
using Dominio.Service.Base;
using Dominio.Service.Validadores;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Dominio.Service.Servicos
{
    public class DiretorService : ServicoBase, IDiretorService
    {
        private readonly IDiretorRepositorio _repositorio;
        public DiretorService(InjectorServico injector, IDiretorRepositorio repositorio) : base(injector)
        {
            _repositorio = repositorio;
        }

        public async Task<Guid?> AdicionarAsync(Diretor diretor)
        {
            if (!Injector.Validador.Executar(new DiretorValidador(), diretor)) return (Guid?)null;
            var diretoDataBase = (await _repositorio.BuscarAsync(x => x.Nome.ToLower() == diretor.Nome.ToLower())).FirstOrDefault();
            if (diretoDataBase != null)
                return diretoDataBase.Id;
            await _repositorio.AdicionarAsync(diretor);
             await base.CommitAsync();
            return diretor.Id;
        }
    }
}
