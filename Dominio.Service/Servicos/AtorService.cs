﻿using Crosscuting.Extensoes;
using Dominio.Contratos.Repositorios;
using Dominio.Contratos.Servicos;
using Dominio.Entidades;
using Dominio.Service.Base;
using Dominio.Service.Validadores;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dominio.Service.Servicos
{
    public class AtorService : ServicoBase, IAtorService
    {
        private readonly IAtorRepositorio _repositorio;
        public AtorService(InjectorServico injector, IAtorRepositorio repositorio) : base(injector)
        {
            _repositorio = repositorio;
        }
        public async Task<IEnumerable<Guid>> AdicionarAsync(List<Ator> atores)
        {
            if (!Injector.Validador.Executar(new AtorListValidador(), atores)) return null;
            var nomesAtores = atores.Select(x => x.Nome.ToLower());
            var atoresExistentes = (await _repositorio.BuscarAsync(x => nomesAtores.Contains(x.Nome.ToLower()))).ToList();
            if (atoresExistentes.HasValue() && atores.TrueForAll(naoExiste => atoresExistentes.Any(existe => naoExiste.Nome.ToLower() == existe.Nome.ToLower())))
                return atoresExistentes.Select(x => x.Id);
            var atoresNaoRegistrados = atores.Where(recebidos => !atoresExistentes.Any(existe => existe.Nome.ToLower() == recebidos.Nome.ToLower()));
            await _repositorio.AdicionarAsync(atoresNaoRegistrados);
            await base.CommitAsync();
            atoresExistentes.AddRange(atoresNaoRegistrados);
            return atoresExistentes.Select(x => x.Id);
        }
    }
}
