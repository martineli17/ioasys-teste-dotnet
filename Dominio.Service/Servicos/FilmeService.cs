﻿using Dominio.Contratos.Repositorios;
using Dominio.Contratos.Servicos;
using Dominio.DTO;
using Dominio.Entidades;
using Dominio.Service.Base;
using Dominio.Service.Validadores;
using Dominio.Service.Validadores.Base;
using Dominio.TiposValores;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dominio.Service.Servicos
{
    public class FilmeService : ServicoBase, IFilmeService
    {
        private readonly Lazy<IFilmeRepositorio> _repositorio;
        private readonly Lazy<IAvaliacaoRepositorio> _avaliacaoRepositorio;
        public FilmeService(InjectorServico injector, Lazy<IFilmeRepositorio> repositorio, Lazy<IAvaliacaoRepositorio> avaliacaoRepositorio)
            : base(injector)
        {
            _avaliacaoRepositorio = avaliacaoRepositorio;
            _repositorio = repositorio;
        }

        public async Task AdicionarAsync(Filme filme)
        {
            if (!Injector.Validador.Executar(new FilmeValidador(), filme)) return;
            if (await _repositorio.Value.ExisteAsync(x => x.Nome.ToLower() == filme.Nome.ToLower()))
            {
                base.Injector.Notificador.Inserir("Filme já cadastrado.");
                return;
            }
            await _repositorio.Value.AdicionarAsync(filme);
            await base.CommitAsync();
        }

        public async Task AvaliarAsync(Guid idFilme, EnumAvaliacaoFilme nota)
        {
            var avalicacao = new Avaliacao { IdFilme = idFilme, Nota = nota };
            if (!Injector.Validador.Executar(new AvaliacaoValidador(), avalicacao)) return;
            if (!await _repositorio.Value.ExisteAsync(x => x.Id == idFilme))
            {
                Injector.Notificador.Inserir(MensagensValidador.NotFound);
                return;
            }
            await _avaliacaoRepositorio.Value.AdicionarAsync(avalicacao);
            await base.CommitAsync();
        }

        public async Task<IEnumerable<FilmeResumidoDTO>> BuscarResumidoPorFiltroAsync(FilmeFiltroDTO filtro)
        {
            var filmes = (await _repositorio.Value.BuscarPorFiltroAsync(filtro, false)).AsEnumerable();
            return Injector.Mapper.Map<IEnumerable<FilmeResumidoDTO>>(filmes);
        }

        public async Task<IEnumerable<FilmeDetalhadoDTO>> BuscarDetalhadoPorFiltroAsync(FilmeFiltroDTO filtro)
        {
            var filmes = (await _repositorio.Value.BuscarPorFiltroAsync(filtro, true)).AsEnumerable();
            return Injector.Mapper.Map<IEnumerable<FilmeDetalhadoDTO>>(filmes);
        }
    }
}
