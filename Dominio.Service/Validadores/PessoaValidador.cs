﻿using Dominio.Entidades.Base;
using Dominio.Service.Validadores.Base;
using FluentValidation;

namespace Dominio.Service.Validadores
{
    public class PessoaValidador : AbstractValidator<Pessoa>
    {
        public PessoaValidador()
        {
            RuleFor(x => x.Nome).NotEmpty().WithMessage(MensagensValidador.NotNullGeneric)
                                 .MaximumLength(50).WithMessage(MensagensValidador.MaxLengthInvalid);
        }
    }
}
