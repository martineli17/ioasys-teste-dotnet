﻿using Dominio.Entidades;
using FluentValidation;

namespace Dominio.Service.Validadores
{
    public class DiretorValidador : AbstractValidator<Diretor>
    {
        public DiretorValidador()
        {
            RuleFor(x => x).SetValidator(new PessoaValidador());
        }
    }
}
