﻿using Dominio.Entidades;
using Dominio.Service.Validadores.Base;
using FluentValidation;

namespace Dominio.Service.Validadores
{
    public class AvaliacaoValidador : AbstractValidator<Avaliacao>
    {
        public AvaliacaoValidador()
        {
            RuleFor(x => x.Nota).IsInEnum().NotEmpty().WithMessage(MensagensValidador.NotNullGeneric);
        }
    }
}
