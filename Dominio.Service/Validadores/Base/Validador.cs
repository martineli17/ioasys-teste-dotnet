﻿using Crosscuting.Servicos.Notificadores;
using FluentValidation;
using System.Linq;

namespace Dominio.Service.Validadores.Base
{
    public class Validador : IValidador
    {
        private readonly INotificador _notificador;
        public Validador(INotificador notificador)
        {
            _notificador = notificador;
        }
        public bool Executar<TValidador, TObject>(TValidador validator, TObject objeto) where TValidador : AbstractValidator<TObject>
        {
            var validacao = validator.Validate(objeto);
            if (!validacao.IsValid)
                _notificador.Inserir(validacao.Errors.Select(x => x.ErrorMessage).ToList(), EnumTipoNotificacao.Warning);
            return validacao.IsValid;
        }
    }
}
