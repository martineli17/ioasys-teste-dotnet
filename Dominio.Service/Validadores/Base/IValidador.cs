﻿using FluentValidation;

namespace Dominio.Service.Validadores.Base
{
    public interface IValidador
    {
        bool Executar<TValidador, TObject>(TValidador validator, TObject objeto) where TValidador : AbstractValidator<TObject>;
    }
}
