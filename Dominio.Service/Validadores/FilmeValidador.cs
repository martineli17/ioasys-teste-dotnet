﻿using Dominio.Entidades;
using Dominio.Service.Validadores.Base;
using FluentValidation;
using System;

namespace Dominio.Service.Validadores
{
    public class FilmeValidador : AbstractValidator<Filme>
    {
        public FilmeValidador()
        {
            RuleFor(x => x.Nome).NotEmpty().WithMessage(MensagensValidador.NotNullGeneric)
                                .MaximumLength(50).WithMessage(MensagensValidador.MaxLengthInvalid);
            RuleFor(x => x.Titulo).NotEmpty().WithMessage(MensagensValidador.NotNullGeneric)
                                .MaximumLength(25).WithMessage(MensagensValidador.MaxLengthInvalid);
            RuleFor(x => x.Descricao).NotEmpty().WithMessage(MensagensValidador.NotNullGeneric)
                                .MaximumLength(500).WithMessage(MensagensValidador.MaxLengthInvalid);
            RuleFor(x => x.DataLancamento).LessThanOrEqualTo(DateTime.Now).WithMessage("Data de Lançamento inválida. Deve ser inferior a data atual")
                                .When(x => x.DataLancamento.HasValue);
            RuleFor(x => x.IdadeRecomendada).InclusiveBetween(0,18).WithMessage("Idade recomendada inválida. Permitido 0 até 18 anos");
            RuleFor(x => x.Genero).IsInEnum();
        }
    }
}
