﻿using Dominio.Entidades;
using FluentValidation;
using System.Collections.Generic;

namespace Dominio.Service.Validadores
{
    public class AtorValidador : AbstractValidator<Ator>
    {
        public AtorValidador()
        {
            RuleFor(x => x).SetValidator(new PessoaValidador());
        }
    }
    public class AtorListValidador : AbstractValidator<List<Ator>>
    {
        public AtorListValidador()
        {
            RuleForEach(x => x).SetValidator(new PessoaValidador());
        }
    }
}
