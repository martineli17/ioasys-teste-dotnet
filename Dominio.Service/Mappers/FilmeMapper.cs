﻿using AutoMapper;
using Crosscuting.Extensoes;
using Dominio.DTO;
using Dominio.Entidades;
using System;
using System.Collections.Generic;

namespace Dominio.Service.Mappers
{
    public class FilmeMapper : Profile
    {
        public FilmeMapper()
        {
            CreateMap<FilmeDTO, Filme>()
                .ForMember(dest => dest.Atores, options => options.MapFrom(src => (IEnumerable<Ator>)null))
                .ForMember(dest => dest.Diretor, options => options.MapFrom(src => (Diretor)null))
                .AfterMap((src, dest) => dest.Id = Guid.NewGuid())
                ;
            CreateMap<Filme, FilmeResumidoDTO>();
            CreateMap<Filme, FilmeDetalhadoDTO>()
                .ForMember(dest => dest.Atores, options => options.MapFrom(src => ConvertParaAtores(src.Atores)))
                .ForMember(dest => dest.Diretor, options => options.MapFrom(src => src.Diretor == null ? null : src.Diretor.Nome))
                .ForMember(dest => dest.TotalDeVotos, options => options.MapFrom(src => src.ContarTotalDeVotos()))
                .ForMember(dest => dest.Avaliacao, options => options.MapFrom(src => src.CalcularMediaAvalicacao()))
                
                ;
        }
        #region Métodos Privados
        private IEnumerable<string> ConvertParaAtores(IEnumerable<AtorFilme> atoresFilme)
        {
            if(!atoresFilme.HasValue()) return null;
            var atoresRetorno = new List<string>();
            foreach (var atorFilme in atoresFilme)
                atoresRetorno.Add(atorFilme.Ator?.Nome);
            return atoresRetorno;
        }
        #endregion
    }
}
